""" Common SimpleITK wrappers/functions """

import SimpleITK as sitk
import numpy as np


def otsu_segmentation(img_sitk):
    """ Perform the Otsu segmentation of a given SimpleITK image.

    :param img_sitk: Input image (SimpleITK image).
    :return: SimpleITK image of the Otsu segmentation.
    """
    otsu_filter = sitk.OtsuThresholdImageFilter()
    mask = otsu_filter.Execute(img_sitk)
    return mask


def erode(sitk_img, times=1):
    """ Given a SimpleITK image, erode it according to the times parameter

    :param sitk_img: SimpleITK binary image.
    :param times: Number of erosions performed.
    :return:
    """
    for _ in range(times):
        sitk_img = sitk.ErodeObjectMorphology(sitk_img)
    return sitk_img


def dilate(sitk_img, times=1):
    """ Given a SimpleITK image, dilate it according to the times parameter

    :param sitk_img: SimpleITK binary image.
    :param times: Number of dilations performed.
    :return:
    """
    for _ in range(times):
        sitk_img = sitk.DilateObjectMorphology(sitk_img)
    return sitk_img


def close(sitk_img, times=1):
    """ Given a SimpleITK image, close it according to the times parameter

    :return: SimpleITK image with the largest CC.
    :param sitk_img: SimpleITK binary image.
    :param times: Number of closings performed.
    :return:
    """
    for _ in range(times):
        sitk_img = sitk.BinaryMorphologicalClosing(sitk_img)
    return sitk_img


def sort_dict_by_val(labels: dict, filt: float = 0., top=-1):
    """ Sort dict based on the values and remove the smaller items

    It will sort the whole dict and then remove those key-value pairs whose
    value is smaller than filt * the_first_value.

    The top parameter keeps the N biggest values. This option overrides the
    filt parameter.
    """
    # Sort descending by value
    labels = {k: v for k, v in sorted(labels.items(),
                                      key=lambda item: item[1],
                                      reverse=True)}
    if filt == 0:
        return labels

    i = 0
    if top == -1:
        for i, val in enumerate(labels.values()):  # Determine cut index (i)
            if val < filt * list(labels.values())[0]:
                break
    elif top == 0:
        return {}
    elif top < 0:
        raise AttributeError(f"A negative value for the 'top' parameter was"
                             f"provided ({top}).")
    else:
        i = top

    # Create the dict with the first i elements only
    return {k: v for k, v in zip(list(labels.keys())[:i],
                                 list(labels.values())[:i])}


def get_largest_cc(image, rel=1, top=-1):
    """ Retains the biggest components of a mask.

    It obtains the largest connected components, and according to the rel
    parameter, it draws the components that are at least rel % of the size
    of the biggest CC.

    If the top parameter is set (different than -1), it will override the rel
    parameter and take the biggest N connected components.
    """

    image = sitk.Cast(image, sitk.sitkUInt32)  # Cast to uint32

    connected_component_filter = sitk.ConnectedComponentImageFilter()
    objects = connected_component_filter.Execute(image)

    labels = {}  # Save label id -> size
    # If there is more than one connected component
    if connected_component_filter.GetObjectCount() > 1:
        objects_data = sitk.GetArrayFromImage(objects)

        # Detect the largest connected component
        for i in range(1, connected_component_filter.GetObjectCount() + 1):
            component_data = objects_data[objects_data == i]
            labels[i] = len(component_data.flatten())  # Voxel count

        f_labels = sort_dict_by_val(labels, filt=rel, top=top)
        data_aux = np.zeros(objects_data.shape, dtype=np.uint8)
        for label in f_labels.keys():
            data_aux[objects_data == label] = 1

        # Save edited image
        output = sitk.GetImageFromArray(data_aux)
        output.SetSpacing(image.GetSpacing())
        output.SetOrigin(image.GetOrigin())
        output.SetDirection(image.GetDirection())
    else:
        output = image

    return output