import os

import configparser

DEFAULT_WORKSPACE_PATH = os.path.expanduser('~/headctools')
WORKSPACE_PATH = DEFAULT_WORKSPACE_PATH

# Check if workspace path was set different than default
cfg_file = os.path.expanduser('~/.hctls.ini')
if os.path.exists(cfg_file):
    reader = configparser.ConfigParser()
    reader.read(cfg_file)
    if 'workspace_path' in reader['DEFAULT']:
        WORKSPACE_PATH = reader['DEFAULT']['workspace_path']

os.makedirs(WORKSPACE_PATH, exist_ok=True)


def set_workspace(folder_path: str):
    """ Change the workspace path and save the new path in  ~/.hctls.ini.

    :param folder_path: New workspace path
    """
    folder_path = os.path.expanduser(folder_path)
    os.makedirs(folder_path, exist_ok=True)

    writer = configparser.ConfigParser()
    writer.read(cfg_file)
    # print(config['DEFAULT']['path'])     # -> "/path/name/"
    # config['DEFAULT']['path'] = '/var/shared/'    # update
    writer['DEFAULT']['workspace_path'] = folder_path

    with open(cfg_file, 'w') as configfile:
        writer.write(configfile)
        print(f'Workspace path set to {folder_path}.')
