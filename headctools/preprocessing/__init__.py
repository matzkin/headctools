""" Wrapper functions used to preprocess data. """

from .Preprocessor import Preprocessor


def ct_skull(input_path: str, output_path: str = None,
             params: dict = None):
    """ CT -> skull preprocessing

    :param input_path: Input folder containing the CT images.
    :param output_path: Output path.
    :param params: Parameters for the preprocessing.
    :return:
    """
    preprocessor = Preprocessor(input_path, output_path, 'ct_to_skull',
                                params=params)
    preprocessor.run()
    return preprocessor.save_path
