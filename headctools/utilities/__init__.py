import csv
import os
import os.path as osp
import random
import tempfile
from abc import abstractmethod
from pathlib import Path

import SimpleITK as sitk
import numpy as np
import torch
from raster_geometry import cylinder, cube

from . import sitkfunc


class FileFolderIterator:
    def __init__(self, input_ff, out_path=None, alt_o_name='processed',
                 exts=('.nii.gz', '.nrrd'), title='', ask_suffix=True,
                 include_subfolders=False):
        self.input_ff = input_ff  # Input file or folder path

        # Supported file extensions
        self.exts = [exts] if type(exts) is str else exts
        self.title = title  # Description that will be displayed in run()

        self.alt_o_name = alt_o_name
        self.out_path = out_path
        self.out_folder_path = out_path
        self.created = []

        # Check if a file has one of the extensions
        self.sup_fext = lambda x: any([x.endswith(fe) for fe in self.exts])

        self.ask_suffix = ask_suffix  # Check if there are different file suff
        self.include_subfolders = include_subfolders

    def run(self, force_folder=None):
        """ Method called after object creation that performs the file action.

        If the path is a folder, it will list the files that match the given
        extension and will run the iterate_file method on each one.

        This method has to be called in the last line of the child classes
        __init__ method.
        """
        print(self.title)

        if force_folder and osp.isdir(force_folder):
            print(f"  subfolder: {force_folder}")
            self.iterate_folder(force_folder)

        if not osp.exists(self.input_ff):
            raise FileNotFoundError(f"Input path does not exist. "
                                    f"({self.input_ff})")
        if osp.isdir(self.input_ff):
            print(f"Input folder: {self.input_ff}")
            self.iterate_folder(self.input_ff)
        if osp.isfile(self.input_ff):
            # If the output folder path is not set, infer one.
            alt_out_f = osp.join(osp.split(self.input_ff)[0], self.alt_o_name)
            if not self.out_folder_path:
                self.out_folder_path = alt_out_f
            veri_folder(self.out_folder_path)

            print(f"Input file: {self.input_ff}")
            self.iterate_file(self.input_ff)

    def iterate_folder(self, input_folder):
        """ In case the provided path is a folder, iterate on the listed files.

        :param input_folder: Input folder path.
        """
        alt_out_f = osp.join(input_folder, self.alt_o_name)  # Alternative path
        self.out_folder_path = self.out_path  # Initialize
        if input_folder != self.input_ff:  # Input path is a subfolder
            if self.out_path:  # Output folder set -> must create a subfolder
                comm = os.path.commonprefix([self.out_path, input_folder])
                subf = input_folder[len(comm):]
                self.out_folder_path = osp.join(self.out_path, subf)
        if not self.out_path:  # No output folder set
            self.out_folder_path = alt_out_f  # Set inferred path

        files = []
        folders = []
        for name in os.listdir(input_folder):
            elem_path = osp.join(input_folder, name)
            if os.path.isfile(elem_path) and self.sup_fext(elem_path):
                files.append(elem_path)
            elif self.include_subfolders and os.path.isdir(elem_path):
                folders.append(elem_path)
        if len(files) == 0:
            print(f"No files in {input_folder} that match the supported "
                  f"file extensions ({self.exts}).")

        veri_folder(self.out_folder_path)
        self.created.append(self.out_folder_path)

        if self.ask_suffix:
            ids = unique_ids(files)
            if len(ids) > 1:
                idstr = ', '.join(ids)
                print(f"WARNING: Found more than one suffix in the filenames ("
                      f"{idstr}). All the images in this folder will be "
                      f"processed.")
        for file in files:  # All the files matching the etension
            self.iterate_file(file)
        for folder in folders:  # If include_subfolders is False this is empty
            if folder not in self.created:
                self.iterate_folder(folder)

    @abstractmethod
    def iterate_file(self, input_file):
        pass


def veri_folder(path=None):
    """Verifies if the folder exists, if not, it will be created.

    :param path: folder to be ckecked/created.
    :return: path of the folder
    """
    if not path:
        return None
    else:
        os.makedirs(path, exist_ok=True)
        return path


def dice_coef_per_class_avg(output, masks):
    b_size = masks.size(0)
    num_classes = masks.size(1)
    probs = output
    mask = masks

    loss = []
    eps = 0.0000001
    num = probs.view(b_size, num_classes, -1) * mask.view(
        b_size, num_classes, -1
    )
    den1 = probs.view(b_size, num_classes, -1) * probs.view(
        b_size, num_classes, -1
    )
    den2 = mask.view(b_size, num_classes, -1) * mask.view(
        b_size, num_classes, -1
    )
    for c in range(num_classes):
        n = num[:, c].sum(1)
        d1 = den1[:, c].sum(1)
        d2 = den2[:, c].sum(1)
        loss.append(2 * torch.mean(((n + eps) / (d1 + d2 + eps))))
    return sum(loss) / len(loss)


def get_max_dims(img_folder, default_dims=None, ext=".nii.gz"):
    """ Get max size for each dimension of all the imgs in a folder.

    Given a folder with .nii.gz images, returns the biggest sizes in each
    dimension. Useful for adding padding in NN that require all the images
    to be the same size

    :param img_folder:
    :param default_dims:
    :return:
    """
    if default_dims:
        dims = default_dims
    else:
        dims = [0, 0, 0]

    for root, dirs, files in os.walk(img_folder):  # Files and subfold
        for i, name in enumerate(sorted(files, key=len)):
            if name.endswith(ext):
                filepath = osp.join(root, name)  # Reconstruct file path.
                img = sitk.ReadImage(filepath)

                img_size = np.array(img.GetSize())
                dims[0] = max(dims[0], img_size[0])
                dims[1] = max(dims[1], img_size[1])
                dims[2] = max(dims[2], img_size[2])
    return dims


def create_csv(data_folder, csv_name="files.csv", splits=None,
               image_identifier=None, mask_identifier=None,
               image_extension=".nii.gz", include_path=True):
    """

    Create csv file of images in data_folder. In that folder, a "files.csv"
    will be created.

    :param data_folder: Input folder
    :param csv_name: Output csv name
    :param splits: Ratio of the train/val or train/val/test splits.
    :param image_identifier: ID of the images in the filenames.
    :param mask_identifier: ID of the masks in the filenames.
    :param image_extension: Image extension that will be used.
    :param include_path: Use absolute path inside the csv file.
    :return:
    """
    print("Generating CSV file. Saving as: ", csv_name)

    if splits is not None:
        splits = None if np.sum(splits) <= 0 or np.sum(splits) > 1 else splits

    filelist = [
        (
            (image_identifier if image_identifier else "image"),
            (mask_identifier if mask_identifier else "mask"),
        )
    ]

    for name in os.listdir(data_folder):
        f_ext = osp.splitext(name)[1]
        if f_ext in image_extension:
            if mask_identifier and mask_identifier not in name:
                if image_identifier:
                    mask_name = name.replace(image_identifier, mask_identifier)
                    if include_path:
                        filepath = osp.join(data_folder, name)
                        filepath_m = osp.join(data_folder, mask_name)
                    else:
                        filepath = name
                        filepath_m = mask_name
                else:
                    mask_name = name.replace(f_ext,
                                             "_" + mask_identifier + f_ext)
                    if include_path:
                        filepath = osp.join(data_folder, name)
                        filepath_m = osp.join(data_folder, mask_name)
                    else:
                        filepath = name
                        filepath_m = mask_name
                filelist.append((filepath, filepath_m))
            elif mask_identifier and mask_identifier in name:
                continue
            elif not mask_identifier:
                filepath = osp.join(data_folder,
                                    name) if include_path else name
            else:
                filepath = osp.join(data_folder,
                                    name) if include_path else name

                name_m = name.replace(f_ext, "_" + mask_identifier + f_ext)
                filepath_m = osp.join(data_folder,
                                      name_m) if include_path else name
            filelist.append((filepath, filepath_m))
        else:
            continue  # Not an image file

    if splits is not None:
        if len(splits) == 2:
            cut_idx = int(splits[0] * len(filelist))
            train_lst = filelist[
                        1:cut_idx
                        ]  # Split the list previously created
            test_lst = filelist[cut_idx:]
        elif len(splits) == 3:  # Train/validation/test splits
            cut_idx_1 = int(len(filelist) * splits[0])
            cut_idx_2 = int(len(filelist) * (splits[0] + splits[1]))
            train_lst = filelist[
                        1:cut_idx_1
                        ]  # Split the list previously created
            validation_lst = filelist[cut_idx_1:cut_idx_2]
            test_lst = filelist[cut_idx_2:]
        else:
            print("Wrong splits dimensions")
            return None

        train_lst.insert(
            0, (image_identifier, mask_identifier)
        )  # Insert headers.
        test_lst.insert(0, (image_identifier, mask_identifier))
        train_name = csv_name.replace(".csv", "_train.csv")
        test_name = csv_name.replace(".csv", "_test.csv")
        train_path = osp.join(data_folder, train_name)
        test_path = osp.join(data_folder, test_name)

        if len(splits) == 3:
            validation_lst.insert(0, (image_identifier, mask_identifier))
            validation_name = csv_name.replace(".csv", "_validation.csv")
            validation_path = osp.join(data_folder, validation_name)
            with open(validation_path, "w") as fp:  # Save validation CSV file
                writer = csv.writer(fp, delimiter=",")
                writer.writerows(validation_lst)
            splits_path = [train_path, validation_path, test_path]

        with open(train_path, "w") as fp:  # Save train CSV file
            writer = csv.writer(fp, delimiter=",")
            writer.writerows(train_lst)

        with open(test_path, "w") as fp:  # Save test CSV file
            writer = csv.writer(fp, delimiter=",")
            writer.writerows(test_lst)

        if len(splits) == 2:
            splits_path = [train_path, test_path]

    csv_path = osp.join(data_folder, csv_name)
    with open(csv_path, "w") as fp:  # Save all files CSV
        writer = csv.writer(fp, delimiter=",")
        writer.writerows(filelist)

    print("Saved: ", csv_path)
    if splits is not None:
        print("Saved: ", splits_path)
        return csv_path, splits_path

    return csv_path


def simple_csv(data_folder, csv_name="files.csv", image_identifier=None,
               mask_identifier=None, ext='.nii.gz'):
    """ Create a csv file listing the files inside a directory.

    :param data_folder: Input folder.
    :param csv_name: Output csv name.
    :param image_identifier: ID of the images in the filenames.
    :param mask_identifier: ID of the masks in the filenames.
    :param ext: Image extension that will be used.
    """
    # Add header
    file_list = [((image_identifier if image_identifier else "image"),
                  (mask_identifier if mask_identifier else "mask"),)]

    # List files and filter by file extension
    files = [osp.join(data_folder, f) for f in os.listdir(data_folder) if
             f.endswith(ext)]
    if image_identifier:  # Filter files if img ID provided
        files = [f for f in files if image_identifier in f]

    for path in files:
        # If mask ID provided, add path without checking if it exists
        if mask_identifier:
            file_list.append((path, path.replace(image_identifier,
                                                 mask_identifier)))
        else:  # Otherwise, leave an empty mask path
            file_list.append((path, ''))

    csv_path = osp.join(data_folder, csv_name)
    with open(csv_path, "w") as fp:  # Save all files CSV
        writer = csv.writer(fp, delimiter=",")
        writer.writerows(file_list)

    print("Saved: ", csv_path)
    return csv_path


def salt_and_pepper(
        img, noise_probability=1, noise_density=0.2, salt_ratio=0.1
):
    batch_size = img.shape[0]
    output = np.copy(img).astype(np.uint8)
    noise_density = np.random.uniform(0, noise_density)
    for i in range(batch_size):
        r = random.uniform(0, 1)  # Random number
        if noise_probability >= r:  # Inside the probability
            blackDots = (
                    np.random.uniform(0, 1, output[i, :, :, :].shape)
                    > noise_density * (1 - salt_ratio)
            ).astype(np.uint8)
            whiteDots = 1 - (
                    np.random.uniform(0, 1, output[i, :, :, :].shape)
                    > noise_density * salt_ratio
            ).astype(np.uint8)
            output[i, :, :, :] = np.logical_and(output[i, :, :, :], blackDots)
            output[i, :, :, :] = np.logical_or(output[i, :, :, :], whiteDots)
    return output


def skull_random_hole(img, prob=1, return_extracted=False):
    """ Simulate craniectomies placing random binary shapes.

    Given a batch of 3D images (PyTorch tensors), crop a random cube or box
    placed in a random position of the image with the sizes given in d.

    :param img: Input image.
    :param prob: probability of adding the noise (by default flip a coin).
    :param return_extracted: Return extracted bone flap.
    """
    is_tensor = True if type(img) == torch.Tensor else False
    if is_tensor:  # Batch of PyTorch tensors
        batch_size = img.shape[0]
        output = np.copy(img).astype(np.uint8)
        if return_extracted:
            flap = np.copy(img).astype(np.uint8)
        for i in range(batch_size):
            np_img = output[i, :, :, :]
            if not return_extracted:
                output[i] = random_blank_patch(np_img, prob, return_extracted)
            else:
                output[i], flap[i] = random_blank_patch(np_img, prob,
                                                        return_extracted)
        output = output if not is_tensor else torch.tensor(output,
                                                           dtype=torch.int8)
        if not return_extracted:
            return output
        else:
            flap = flap if not is_tensor else torch.tensor(flap,
                                                           dtype=torch.int8)
            return output, flap
    else:  # Single image of Preprocessor class
        np_img = sitk.GetArrayFromImage(img)
        o_spacing = img.GetSpacing()
        o_direction = img.GetDirection()
        o_origin = img.GetOrigin()

        np_img = np_img.astype(np.uint8)
        c_img = random_blank_patch(np_img, prob)

        img_o = sitk.GetImageFromArray(c_img)
        img_o.SetSpacing(o_spacing)
        img_o.SetOrigin(o_origin)
        img_o.SetDirection(o_direction)
        return img_o


def shape_3d(center, size, image_size, shape="flap"):
    """ Generate a 3D shape from a given img size and center.

    Return a 3D numpy sphere or cube given the center, size and image size.
    It creates a mask of the shape using the p-norm concept.

    :param center: array with the coordinates center of the shape.
    :param size: single number that represents the size of the shape in each
    dimension.
    :param image_size: size of the output array.
    :param shape: shape to return. Currently 'circle' and 'cube' are allowed.
    :return:
    """
    if type(image_size) == sitk.Image:
        image_size = sitk.GetArrayFromImage(image_size).shape

    if shape in ["circle", "sphere"]:
        ord = 2
    elif shape in ["square", "box", "cube"]:
        ord = np.inf
    elif shape in ["flap", "autoimplant"]:
        c_diam = (
                np.random.uniform(0.25, 1) * size / 4
        )
        center_relative = tuple(l / r for l, r in zip(center, image_size))
        z_edge_1 = (
            (center[0]) / image_size[0],
            (center[1] - size / 2) / image_size[1],
            (center[2] - size / 2) / image_size[2],
        )
        z_edge_2 = (
            (center[0]) / image_size[0],
            (center[1] - size / 2) / image_size[1],
            (center[2] + size / 2) / image_size[2],
        )

        cyl1 = cylinder(image_size, size, c_diam, 0, z_edge_1).astype(np.uint8)
        cyl2 = cylinder(image_size, size, c_diam, 0, z_edge_2).astype(np.uint8)
        cub1 = cube(image_size, size, center_relative).astype(np.uint8)

        mask = np.logical_or(cyl1, np.logical_or(cyl2, cub1)).astype(np.uint8)
        return 1 - mask
    else:
        print(
            "Shape {} is not supported. Setting shape as sphere".format(shape)
        )
        ord = 2
    distance = np.linalg.norm(
        np.subtract(np.indices(image_size).T, np.asarray(center)),
        axis=len(center),
        ord=ord,
    )
    shape_np = 1 - np.ones(image_size).T * (distance <= size)
    return shape_np.T


def random_blank_patch(image, prob=1, return_extracted=False, p_type="random"):
    r = random.uniform(0, 1)  # Random number
    if prob >= r:  # Inside the probability -> crop
        image_size = image.shape

        while True:
            # Define center of the mask
            center = np.array(
                [np.random.randint(0, dim) for dim in image.shape]
            )  # random point
            plane_cond = (
                    center[1] * (3 / 7 * image_size[0] / image_size[1]) +
                    center[0]
                    > 0.65 * image_size[0]
            )  # Plane
            if image[tuple(center)] and plane_cond:  # white pixel
                break

        # Define radius
        min_radius = (np.min(image_size) // 5) - 1
        max_radius = np.max([min_radius, np.max(image_size) // 3.5])
        size = np.random.randint(min_radius, max_radius)

        valid_shapes = ["sphere", "box", "flap"]
        p_type = (
            valid_shapes[np.random.randint(0, len(valid_shapes))]
            if p_type not in valid_shapes
            else p_type
        )
        if p_type == "sphere":
            shape_np = shape_3d(center, size, image_size, shape="sphere")
        elif p_type == "box":
            shape_np = shape_3d(center, size, image_size, shape="box")
        else:
            shape_np = shape_3d(center, size, image_size, shape="flap")

        # Mask the image
        masked_out = np.logical_and(image, shape_np).astype(
            np.uint8
        )  # Apply the mask

        if not return_extracted:
            return masked_out
        else:
            extracted = np.logical_and(image, 1 - shape_np).astype(np.uint8)
            return masked_out, extracted
    else:
        if not return_extracted:
            return image
        else:
            return image, np.zeros_like(image)


def diff_sitk(im_a, im_b):
    """ Perform the difference between two SimpleITK images.

    :param im_a: Input image.
    :param im_b: Image to use as subtrahend.
    :return: Difference image.
    """
    im_a = sitk.Cast(im_a, sitk.sitkInt8)
    im_b = sitk.Cast(im_b, sitk.sitkInt8)
    result = sitk.Cast(sitk.And(sitk.Not(im_a), im_b), sitk.sitkFloat32)
    result.SetOrigin(im_a.GetOrigin())
    result.SetDirection(im_a.GetDirection())
    result.SetSpacing(im_a.GetSpacing())
    return result


def erode(sitk_img, times=1):
    """ Given a SimpleITK image, erode it according to the times parameter

    :param sitk_img: SimpleITK binary image.
    :param times: Number of erosions performed.
    :return: Eroded SimpleITK image.
    """
    for _ in range(times):
        sitk_img = sitk.ErodeObjectMorphology(sitk_img)
    return sitk_img


def dilate(sitk_img, times=1):
    """ Given a SimpleITK image, dilate it according to the times parameter

    :param sitk_img: SimpleITK binary image.
    :param times: Number of dilations performed.
    :return: Dilated SimpleITK image.
    """
    for _ in range(times):
        sitk_img = sitk.DilateObjectMorphology(sitk_img)
    return sitk_img


def erode_dilate(inp_img, p=1, min_it=0, max_it=1):
    """ Apply an image erosion/dilation with a probability of application p.

    :param inp_img: Input image. It could be a torch.Tensor, np.ndarray or
    sitk.Image.
    :param p: Probability for applying the transform.
    :param min_it: Minimum number of iterations.
    :param max_it: Minimum number of iterations.
    :return: Eroded or dilated image in the same image tool.
    """
    if np.random.rand() > p:  # do nothing
        return inp_img

    is_tensor = is_array = False
    if type(inp_img) == torch.Tensor:
        is_tensor = True
        img = inp_img.numpy()
        img = sitk.GetImageFromArray(img)
    elif type(inp_img) == np.ndarray:
        is_array = True
        img = sitk.GetImageFromArray(inp_img)

    times = np.random.randint(min_it, max_it)
    out_img = np.random.choice([erode, dilate])(img, times)

    if is_array:
        return sitk.GetArrayFromImage(out_img)
    elif is_tensor:
        return torch.tensor(sitk.GetArrayFromImage(out_img))
    else:
        return out_img


def str_to_num(text, max_n=11):
    """
    Get a number from a string, based on the characters numeric code and a custom maximum set number.

    :param text: Input string.
    :param max_n: (One plus) the maximum number allowed.
    :return: Output number, in the range [0, max_n).
    """
    s = 0
    for c in text:
        s += ord(c)
    return s % max_n


def crop_str_from_n_occurrence(inp_str, ch="_", n=2):
    """ Crop a string after the nth occurrence

    Get a substring finding the nth occurrence of ch and slicing up to that
    position.
    Example: "image_to_process_training.png" with ch='_' and n=2 -> "image_to"

    :param inp_str: Input string.
    :param ch: Separator character to find.
    :param n: Number of separators in the output substring.
    :return: Substring
    """
    idxs = [pos for pos, char in enumerate(inp_str) if char == ch]
    if len(idxs) == 0:  # char not found
        return inp_str
    idx = n if n <= len(idxs) else len(idxs)
    return inp_str[: idxs[idx - 1]]


def assert_avail(param, valid, name='parameter'):
    """ Assert that param is among valid or raise an AttributeError.

    In case it doesn't match the valid parameters it raises an AttributeError
    listing the valid parameters.

    :param param: String containing a parameter to check.
    :param valid: List of the available parameters.
    :param name: Name of the parameter (e.g. 'method', model', 'name').
    """
    valid_str = ', '.join(valid)
    if param not in valid:
        raise AttributeError(f"The {name} you entered is not valid "
                             f"({param}.).\nAvailable {name}s are: "
                             f"{valid_str}.")


def grab_exts(path):
    """ Return the file extensions of a file path.

    Differently from os.path.splitext, this function will return multiple
    file extensions if it's the case (useful for .nii.gz images).

    :param path: File path.
    :return: File extensions.
    """
    return ''.join(Path(path).suffixes)


def unique_ids(words: list, has_ext: bool = True):
    """ Return a list of the common ids in a list of strings

    Given a list of strings, it returns the ids that are common in at least
    two strings of that list.

    Example: ['abc_de', 'zxc_de', 'abc_fr', 'zxc_jk', 'abc_jk', 'zxc_jk', ]
    produces ['c_de', 'c_jk']

    :param words: list of strings to be analyzed.
    :param has_ext: if True, it won't consider the file extension.
    :return:
    """
    words_ext = words.copy()
    ids = []
    for i, word in enumerate(words):
        ext = grab_exts(word) if has_ext else ''
        word_ne = word[:-len(ext)] if has_ext else word
        for j, elem in enumerate(words_ext):
            if word == elem:
                continue
            elem_ne = elem[:-len(ext)] if has_ext else elem
            suff = os.path.commonprefix([word_ne[::-1], elem_ne[::-1]])[::-1]
            if suff:
                if '_' in suff:  # ['c_de', 'c_jk'] -> ['de', 'jk']
                    suff = '_'.join(suff.split('_')[1:])
                ids.append(suff)
    return list(set(ids))


def remove_if_temp(paths):
    """ Remove temporary files if they are in the temp folder.

    :param paths: It could be a list of paths or a single path.
    :return:
    """
    if type(paths) is str:
        paths = [paths]
    for path in paths:
        if path.startswith(tempfile.gettempdir()):
            os.remove(path)
