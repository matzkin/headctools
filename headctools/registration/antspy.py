import os
import tempfile
from os import path as osp

import SimpleITK as sitk
import ants

from ..utilities import veri_folder, remove_if_temp


def register_antspy(moving_img_path, fixed_image, out_image=None,
                    mat_path=None, transformation="AffineFast",
                    overwrite=False, binary=False, clean_up=False):
    atlas_name = osp.split(fixed_image)[1].split(".")[0]
    mv_img_p, mv_img_n = osp.split(moving_img_path)

    out_dir = osp.join(mv_img_p, atlas_name)
    veri_folder(out_dir)

    out_image = (
        osp.join(out_dir, mv_img_n) if out_image is None else out_image
    )

    mat_path = (
        osp.join(out_dir, mv_img_n.replace(".nii.gz", "_reg.mat"))
        if mat_path is None
        else mat_path
    )

    if osp.exists(out_image) and not overwrite and osp.getsize(out_image) > 0:
        print("\n  The output file already exists (skipping).")
        return
    else:
        ct = ants.image_read(moving_img_path)
        fixed = ants.image_read(fixed_image)

        my_tx = ants.registration(fixed=fixed, moving=ct,
                                  type_of_transform=transformation)
        transf_ct = my_tx['warpedmovout']
        reg_transform = my_tx['fwdtransforms']
        ants.write_transform(ants.read_transform(reg_transform[0]), mat_path)
        transf_ct = transf_ct > .5 if binary else transf_ct
        ants.image_write(transf_ct, out_image)

    if clean_up:
        remove_if_temp([mat_path])


def apply_mat(moving_img_path, out_image, mat_path, reference_path,
              interp="nearestNeighbor", overwrite=False, invert=False):
    """ Apply a saved transformation to a moving image.


    :param moving_img_path: Input image path.
    :param out_image: Output image path.
    :param mat_path: Transform path.
    :param reference_path: Image used as reference for determining dimensions.
    :param interp: Interpolation method used.
    :param overwrite: Overwrite the output image if it already exists.
    :return:
    """
    if osp.exists(out_image) and not overwrite and osp.getsize(out_image) > 0:
        print("already exists (skipping).")
        return

    moving_img = ants.image_read(moving_img_path)
    reference = ants.image_read(reference_path)

    transf_img = ants.apply_transforms(fixed=reference, moving=moving_img,
                                       transformlist=[mat_path],
                                       interpolator=interp,
                                       whichtoinvert=[invert])

    ants.image_write(transf_img, out_image)

    print("     pred in orig space saved in {}.".format(out_image))


def register_sitk(moving_image, fixed_image=None, mat_save_path=None,
                  transform="Rigid", interp="nearestNeighbor",
                  mat_to_apply=None, reference=None, clean_up=False):
    """ Register images using ANTsPy and SimpleITK.

    :param moving_image: Image to register.
    :param fixed_image: Atlas image.
    :param mat_save_path: Tasnformation parameters path.
    :param transform: Type of transform.
    :param interp: Interpolator used.
    :param mat_to_apply: If given, it will apply this transformation.
    :param reference: If mat_to_apply, reference_path image for applying the
    transform and determining the dimensions.
    :param clean_up: If True, it will delete the temporary files.
    :return:

    transformation can be one of: “Translation”, “Rigid”, “Similarity”,
    “QuickRigid”, “DenseRigid”, “BOLDRigid”, “Affine”, “AffineFast”,
    “BOLDAffine”, “TRSAA”, “ElasticSyN”, “SyN”, “SyNRA”, “SyNOnly”, “SyNCC”,
    “SyNabp”, “SyNBold”, “SyNBoldAff”, “SyNAggro”, “TVMSQ” or “TVMSQC”.

    interpolator may be:
    linear nearestNeighbor multiLabel for label images but genericlabel is
    preferred gaussian bSpline cosineWindowedSinc welchWindowedSinc
    hammingWindowedSinc lanczosWindowedSinc genericLabel use this for label
    images.

    Find more info about these params in https://antspy.readthedocs.io/en/latest/registration.html  # noqa
    """
    mv_img = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
    mv_img_path = mv_img.name

    sitk.WriteImage(moving_image, mv_img_path)

    out_img = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
    out_img_path = out_img.name

    if fixed_image and type(fixed_image) is not str:
        fx_img = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
        fx_img_path = fx_img.name
        sitk.WriteImage(fixed_image, fx_img_path)
    elif fixed_image:
        fx_img_path = fixed_image

    if mat_to_apply and reference:
        # Used only when applying .mat transform to mask
        ref_img = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
        ref_img_path = ref_img.name
        sitk.WriteImage(reference, ref_img_path)

        apply_mat(mv_img_path, out_img_path, mat_to_apply, ref_img_path,
                  interp, overwrite=False)
    else:
        register_antspy(mv_img_path, fx_img_path, out_img_path,
                        mat_save_path, transform)

    ret_img = sitk.ReadImage(out_img_path)

    if clean_up:  # Remove the files just if they are in the temp folder
        remove_if_temp([mv_img_path, out_img_path, fx_img_path])

    return ret_img


def undo_reg_in_pred(input_ff, prep_id=None, pred_id=None, ext='.nii.gz',
                     interp="nearestNeighbor", overwrite=False, reg_id="",
                     tmp_mode=False):
    """ Take the predictions to the original image space.

    The images must be organized in the following way (each level is a
    subfolder):
        - F1: Images folder in the orig size.
            - F2: Preprocessed images.
                - F3: Predictions folder.

    The transforms will be looked-up in F2, as registration .mat transforms and
    will be reversed before applying them to the images in F3. It will aso
    take the images in F1 as reference.

    :param input_ff: Predictions folder.
    :param prep_id: Preprocessed files identifier. Set this parameter in
    case the preprocessed file names don't match with the original file names,
    and they differ in a suffix, such as "image001.nii" and "image001_prp.nii".
    :param pred_id: Prediction files identifier. Set this parameter in
    case the predictions and preprocessed file names don't match, and they
    differ in a suffix, such as "image001.nii" and "image001_pred.nii".
    :param ext: Image extension.
    :param interp: Interpolation method.
    :param overwrite: Overwrite files.
    :param reg_id: Registered file identifier.

    :return:
    """
    print('\n\n', input_ff)
    if os.path.isdir(input_ff):
        files = [f for f in os.listdir(input_ff) if f.endswith(ext)]
        files = [f for f in files if pred_id in f] if pred_id else files
        input_fo = input_ff
    elif os.path.isfile(input_ff):
        input_fo, input_fi = osp.split(input_ff)
        files = [input_fi]

    prep_folder, _ = osp.split(input_fo)
    orig_folder = osp.split(prep_folder)[0] if not tmp_mode else prep_folder

    for pred_fname in files:
        prep_fname = pred_fname.replace(pred_id, '') if pred_id else pred_fname
        orig_fname = prep_fname.replace(prep_id, '') if prep_id else prep_fname

        mat_fname = prep_fname.replace(ext, reg_id + ".mat")
        mat_fpath = osp.join(prep_folder, mat_fname)
        mat_fpath = mat_fpath if osp.exists(mat_fpath) else osp.join('/tmp',
                                                                     mat_fname)
        ref_fpath = osp.join(orig_folder, orig_fname)
        pred_fpath = osp.join(input_fo, pred_fname)

        out_folder = veri_folder(osp.join(input_fo, "orig_dims"))
        out_fpath = osp.join(out_folder, pred_fname)

        if osp.exists(out_fpath) and overwrite is False:
            print("{} skipped (already exists).".format(out_fpath))
            continue
        elif not osp.exists(mat_fpath) or not osp.exists(ref_fpath):
            print("(pred in orig space) {} skipped. Check that the .mat and "
                  "reference files exist.".format(pred_fname))
            continue
        else:
            apply_mat(pred_fpath, out_fpath, mat_fpath, ref_fpath, interp,
                      overwrite=True, invert=True)


def register_ff(input_ff: str, atlas_path: str, mask_id: str = None,
                overwrite_files: bool = False, binary: bool = False):
    """ Wrapper that checks if the path exists and calls the methods above.

    input_ff: str
        File or folder to register.
    atlas_path: str
        Path of the atlas for registering.
    mask_id: str  # TODO add mask registration after image
        If given, it will register the files with that id with the same
        transformation as the file without the id.
    overwrite_files: bool
        Overwrite files in case of existing.
    """
    if not os.path.exists(input_ff):
        raise AttributeError(
            "The input file/folder wasn't found ({})".format(input_ff)
        )

    files = None
    if os.path.isdir(input_ff):
        s_ext = lambda x: any([x.endswith(e) for e in ['nii.gz', 'nrrd']])
        files = [f for f in os.listdir(input_ff) if s_ext(f)]
    elif os.path.isfile(input_ff):
        files = [input_ff]

    for file in files:
        f_path = os.path.join(input_ff, file)
        print(f"registering {f_path}...")
        register_antspy(
            moving_img_path=f_path,
            fixed_image=atlas_path,
            transformation="AffineFast",
            overwrite=overwrite_files,
            binary=binary
        )
