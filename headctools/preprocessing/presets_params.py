PRESETS = {
    'ct_to_skull': ['register', 'clip_intensity_values', 'normalize',
                    'largest_cc', 'marching_cubes'],
}

DEFAULT_PARAMS = {
    'ct_to_skull': {
        'preset_name': 'ct_to_skull',  # Used for output folder name
        'save_path': None,
        # Registration params
        'reg_atlas_path': '~/headctools/assets/atlas/atlas3_nonrigid_masked_304_224.nii.gz',
        # noqa
        'reg_engine': 'ANTS',  # 'ANTS' / 'deformetrica'
        'reg_im_interp': 'linear',  # Linear, NearestNeighbor..
        'reg_transf': 'Rigid',  # Affine, Rigid..
        'reg_save_transform': True,  # Save transformation matrix
        # Resampling params
        'res_target_size': None,
        'res_spacing': None,  # [1.0, 1.0, 1.0]
        'res_direction': None,
        'res_origin': None,
        # Other params
        'clip_intensity_values': [150, 151],
        'threshold': True,
        'largest_cc': ['image'],  # Labels where to extract the largest CC.
        'fixed_size_pad': None,  # [304, 224, 304]
    }
}
