import math
import os
import os.path
import tempfile
from typing import Optional

import SimpleITK as sitk
import numpy as np
import vtk


def get_niigz_imgs(path: str, file_ids: Optional[dict] = None):
    """From a path get a list of nii.gz images.

    :param path: Input path. It could be a folder or a file.
    :param file_ids: Dictionary of file identifiers.
        example = {
            'image': '_image.nii.gz',
            'mask': '_mask.nii.gz',
        }
    :return: List of dicts of files.
        example = [
            {'image': 'path/to/image1.nii.gz',
             'mask': 'path/to/mask1.nii.gz'},
            {'image': 'path/to/image2.nii.gz',
             'mask': 'path/to/mask2.nii.gz'}
        ]
        If no file_ids are provided, the files are labelled as 'image'.
        example = [
            {'image': 'path/to/image1.nii.gz'},
            {'image': 'path/to/image2.nii.gz'}
        ]
    """
    print(f"Getting nii.gz images from {path}")
    paths, result = [], []
    if os.path.isdir(path):
        for f in os.listdir(path):
            full_path = os.path.join(path, f)
            if os.path.isfile(full_path) and f.endswith('.nii.gz'):
                paths.append(full_path)
    elif os.path.isfile(path) and path.endswith('.nii.gz'):
        paths.append(path)
    else:
        raise FileNotFoundError(
            f"The input path does not contain any valid files ({path})"
        )

    print(f"Found {len(paths)} images")
    list_not_empty = len(paths) > 0
    if file_ids is not None:  # In case we have image, mask (for example)
        while list_not_empty:
            path = paths[0]
            f_name = os.path.split(path)[1]
            for key, value in file_ids.items():  # For each type of image
                if value in f_name:  # Found a valid ID in the file name
                    dc = {}  # Here I'll store image, mask for a subject.
                    for kk in file_ids.keys():  # Grab all IDs and replace them
                        replaced = path.replace(value, file_ids[kk])
                        if not os.path.isfile(replaced):
                            raise FileNotFoundError(
                                f"The file {replaced} does not exists. Base "
                                f"path: {path}, file_ids: {file_ids}. You "
                                f"must rename the files in the folder making "
                                f"them differ only in the ids."
                            )
                        dc[kk] = replaced  # Add the file for that type
                        paths.remove(replaced)  # Remove the file from the list
                        list_not_empty = len(paths) > 0
                    result.append(dc)
    else:
        result = [{'image': p} for p in paths]  # No file_ids provided

    return result


def clip_int_sitk(image, imin, imax):
    """ Clip intensities in a SimpleITK image.

    :param image: Input image.
    :param imin: Minimum intensity value.
    :param imax: Maximum intensity value.
    :return: Clipped image.
    """
    arr = sitk.GetArrayFromImage(image)
    outimg = sitk.GetImageFromArray(np.clip(arr, imin, imax))
    outimg.SetSpacing(image.GetSpacing())
    outimg.SetOrigin(image.GetOrigin())
    outimg.SetDirection(image.GetDirection())
    return outimg


def min_max_normalization(image: sitk.Image):
    """  Map the image intensities to the [0, 1] range.

    :param image: SimpleITK image
    :return: normalized image
    """
    image = sitk.Cast(image, sitk.sitkFloat32)

    arr = sitk.GetArrayFromImage(image)
    return (image - arr.min()) / (arr.max() - arr.min())


def threshold(image: sitk.Image, value: float = 0):
    """ Threshold an image.

    :param image: SimpleITK image
    :param value: Threshold value
    :return: Thresholded image
    """
    pixel_id = image.GetPixelIDValue()
    image = image > value
    sitk.Cast(image, pixel_id)
    return image


def resample_sz_sp(image, mask=None, im_interp='linear', target_spacing=None,
                   direction=None, origin=None, target_size=None):
    """Resample the image size (without deforming the image).

    Resample the image size (without deforming the image) and spacing
    for matching the spacing given as parameter.

    :param target_spacing: desired spacing.
    :param direction: custom direction.
    :param origin: custom origin.
    :param target_size: desired image size.
    :return:
    """
    if not target_spacing:
        if not target_size:
            return None

    if direction is None:
        direction = image.GetDirection()
    if origin is None:
        origin = image.GetOrigin()

    orig_sz = image.GetSize()
    orig_sp = image.GetSpacing()

    t_sz = lambda osz, osp, tsp: int(math.ceil(osz * (osp / tsp)))
    t_sp = lambda osz, osp, tsz: osz * osp / tsz

    if not target_spacing and target_size:
        target_spacing = [t_sp(orig_sz[0], orig_sp[0], target_size[0]),
                          t_sp(orig_sz[1], orig_sp[1], target_size[1]),
                          t_sp(orig_sz[2], orig_sp[2], target_size[2])]

    if not target_size:
        target_size = [t_sz(orig_sz[0], orig_sp[0], target_spacing[0]),
                       t_sz(orig_sz[1], orig_sp[1], target_spacing[1]),
                       t_sz(orig_sz[2], orig_sp[2], target_spacing[2])]

    if im_interp.lower() == 'linear':
        interpolator = sitk.sitkLinear
    elif im_interp.lower() in ['nearest', 'nearestneighbor']:
        interpolator = sitk.sitkNearestNeighbor
    else:
        raise ValueError(f"Unknown interpolator: {im_interp}")

    print("    Image")
    image = sitk.Resample(image, target_size, sitk.Transform(),
                          interpolator, origin, target_spacing,
                          direction, 0.0,
                          image.GetPixelIDValue())

    if mask:
        print("    Mask")
        mask = sitk.Resample(mask, target_size, sitk.Transform(),
                             sitk.sitkNearestNeighbor, origin,
                             target_spacing, direction, 0.0,
                             mask.GetPixelIDValue())

    return image, mask


def fixed_pad(v, final_img_size=None, mode="constant", constant_values=(0, 0),
              return_padding=False):
    """ Add fixed image size padding to an volume v

    :param v: Input 3D image.
    :param final_img_size: Desired image size.
    :param mode: One of the np.pad modes ('constant', 'edge', 'maximum',
    'mean', 'reflect', 'symmetric', 'wrap').
    :param constant_values: Used in 'constant'.  The values to set the padded
    values for each axis
    :param return_padding: Return added padding as output with the result.
    :return: Padded image or tuple containing the padded image with the
    padded size, according to the return_padding option.
    """
    if final_img_size is None:
        print("Desired image size not provided!")
        return None

    for i in range(0, len(final_img_size)):
        if v.shape[i] > final_img_size[i]:
            print("The input size is bigger than the output size!")
            print(v.shape, " vs ", final_img_size)
            return None

    padding = (
        (0, final_img_size[0] - v.shape[0]),
        (0, final_img_size[1] - v.shape[1]),
        (0, final_img_size[2] - v.shape[2]),
    )

    if not return_padding:
        return np.pad(v, padding, mode, constant_values=constant_values)
    else:
        return (
            np.pad(v, padding, mode, constant_values=constant_values),
            padding,
        )


def fixed_pad_sitk(sitk_img, pad):
    """ Add padding to a SimpleITK image.

    :param sitk_img: Input image.
    :param pad: Desired image size.
    :return: Padded image.
    """
    arr = sitk.GetArrayFromImage(sitk_img)
    out_img = sitk.GetImageFromArray(fixed_pad(arr, pad))
    out_img.SetSpacing(sitk_img.GetSpacing())
    out_img.SetOrigin(sitk_img.GetOrigin())
    out_img.SetDirection(sitk_img.GetDirection())
    return out_img


def sort_dict_by_val(labels: dict, filt: float = 0., top=-1):
    """ Sort dict based on the values and remove the smaller items

    It will sort the whole dict and then remove those key-value pairs whose
    value is smaller than filt * the_first_value.

    The top parameter keeps the N biggest values. This option overrides the
    filt parameter.
    """
    # Sort descending by value
    labels = {k: v for k, v in sorted(labels.items(),
                                      key=lambda item: item[1],
                                      reverse=True)}
    if filt == 0:
        return labels

    i = 0
    if top == -1:
        for i, val in enumerate(labels.values()):  # Determine cut index (i)
            if val < filt * list(labels.values())[0]:
                break
    elif top == 0:
        return {}
    elif top < 0:
        raise AttributeError(f"A negative value for the 'top' parameter was"
                             f"provided ({top}).")
    else:
        i = top

    # Create the dict with the first i elements only
    return {k: v for k, v in zip(list(labels.keys())[:i],
                                 list(labels.values())[:i])}


def get_largest_cc(image, rel=1, top=-1):
    """ Retains the biggest components of a mask.

    It obtains the largest connected components, and according to the rel
    parameter, it draws the components that are at least rel % of the size
    of the biggest CC.

    If the top parameter is set (different than -1), it will override the rel
    parameter and take the biggest N connected components.
    """

    image = sitk.Cast(image, sitk.sitkUInt32)  # Cast to uint32

    connected_component_filter = sitk.ConnectedComponentImageFilter()
    objects = connected_component_filter.Execute(image)

    labels = {}  # Save label id -> size
    # If there is more than one connected component
    if connected_component_filter.GetObjectCount() > 1:
        objects_data = sitk.GetArrayFromImage(objects)

        # Detect the largest connected component
        for i in range(1, connected_component_filter.GetObjectCount() + 1):
            component_data = objects_data[objects_data == i]
            labels[i] = len(component_data.flatten())  # Voxel count

        f_labels = sort_dict_by_val(labels, filt=rel, top=top)
        data_aux = np.zeros(objects_data.shape, dtype=np.uint8)
        for label in f_labels.keys():
            data_aux[objects_data == label] = 1

        # Save edited image
        output = sitk.GetImageFromArray(data_aux)
        output.SetSpacing(image.GetSpacing())
        output.SetOrigin(image.GetOrigin())
        output.SetDirection(image.GetDirection())
    else:
        output = image

    return output


def save_img(img, fpath):
    """ Save an image in the given path.

    :param img: Image to save.
    :param fpath: Path to save the image.
    :return:
    """
    os.makedirs(os.path.split(fpath)[0], exist_ok=True)
    sitk.WriteImage(img, fpath)
    print(f"    Saved image in {fpath}")


def nii_to_stl_marching_cubes(input_im_path, output_file=None):
    # If no output path provided
    alt_out_path = input_im_path.replace("nii.gz", "stl")
    output_file = output_file if output_file else alt_out_path

    reader = vtk.vtkNIFTIImageReader()
    reader.SetFileName(input_im_path)
    reader.Update()

    # Marching Cubes
    dmc = vtk.vtkMarchingCubes()
    dmc.SetInputData(reader.GetOutput())
    dmc.GenerateValues(1, 1, 1)
    dmc.Update()

    transform = vtk.vtkTransform()
    qfm = reader.GetQFormMatrix()
    t1 = qfm.GetElement(0, 0) * qfm.GetElement(0, 3)
    t2 = qfm.GetElement(1, 1) * qfm.GetElement(1, 3)
    t3 = qfm.GetElement(2, 2) * qfm.GetElement(2, 3)
    transform.Translate(t1, t2, t3)

    transformPoly = vtk.vtkTransformPolyDataFilter()
    transformPoly.SetInputConnection(dmc.GetOutputPort())
    transformPoly.SetTransform(transform)
    transformPoly.Update()

    # Save the mesh
    writer = vtk.vtkSTLWriter()
    writer.SetInputConnection(transformPoly.GetOutputPort())
    writer.SetFileTypeToBinary()
    writer.SetFileName(output_file)
    writer.Write()

    return output_file


def sitk_to_stl_marching_cubes(sitk_img, output_file=None):
    im = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
    im_path = im.name
    sitk.WriteImage(sitk_img, im_path)
    output_file = nii_to_stl_marching_cubes(im_path, output_file)

    return output_file
