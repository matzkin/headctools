def largest_cc(input_ff: str):
    """ Retain the largest CC of the file or files in the folder

    A Connected Component (CC) is a blob of pixels that can be thought as
    graphs or blobs of a given mask. Running this subcommand

    :param input_ff:
    :return:
    """
