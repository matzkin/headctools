""" Flap reconstruction tool.utils

This uses a ctunet trained model to load a trained model and get the
predictions from it, after preprocessing.
"""
import logging
import os
import os.path as osp
import tempfile
from typing import Optional

import SimpleITK as sitk
import headctools.utilities.sitkfunc as skfn
from SimpleITK import Show as sh
from ctunet.pytorch.train_test import Model

from .. import utilities as common, WORKSPACE_PATH
from ..Preprocessor import prep_img_cr, prep_img_cr_sk
from ..registration.antspy import undo_reg_in_pred
from ..tools.downloader import Downloader

DEVICE = 'cuda'  # Use CUDA if available
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Hide Tensorflow info/warnings


class FlapReconstruction(common.FileFolderIterator):
    def __init__(self, input_ff, out_path=None, exts=('.nii.gz', '.nrrd'),
                 model=None, show_intermediate=False, preprocessing='ct-scan',
                 keep_ratio=1., top=-1, odims=False, mask=None,
                 include_subfolders=False):
        super().__init__(input_ff, out_path, f'pred_{model}', exts,
                         'Flap Reconstruction', False, include_subfolders)
        self.model = model  # Model identifier
        self.sh_imgs = show_intermediate  # Display images with sitk.Show
        self.preprocessing = preprocessing  # Prepr function to use
        self.keep_ratio = keep_ratio  # CC ratio to keep
        self.top = top  # Keep biggest N connected components
        self.odims = odims  # Save the preds in original dimensions
        self.mask = mask  # Mask path to eliminate unwanted structures
        self.input_file = None  # Filename of the input img
        self.mod_fpath = None  # Model path
        self.mod = None  # Model type (UNet)
        self.pro_han = None  # Problem Handler (FlapRec)
        self.out_flap = None  # Predicted flap path
        self.out_inputsk = None  # Input image path
        self.out_fullsk = None  # Predicted skull path
        self.fl_stk = None  # SimpleITK.Image of the predicted flap
        self.inp_stk = None  # SimpleITK.Image of the input
        self.fsk_stk = None  # SimpleITK.Image of the predicted skull
        self.prep_file = None  # Temporary preprocessed file
        self.prep_fpath = None  # Preprocessed file path
        self.prep_file_folder = None
        self.prep_file_name = None
        self.is_sk = None  # True if problemhandler predicts skulls

        self.run()

    def iterate_file(self, input_file):
        self.input_file = input_file
        self.resolve_paths()
        self.preprocess()
        self.run_model()
        self.load_preds()
        self.postprocess()
        self.save_images()

    def resolve_paths(self):
        df = Downloader.get_assets_per_name(self.model)  # Get asset files
        paths = [pth for pth in list(df['path']) if 'model' in pth]
        if len(paths) == 0:
            raise AssertionError(f"The csv for ({self.model}) does not "
                                 f"contain any model path.")
        elif len(paths) > 1:
            logging.info("More than one model path found. The first one will"
                         f"be used ({paths[0]}).")

        # Relative path of the model
        rel_path = paths[0]  # UNet_FlapRec/model/default.pt
        self.mod_fpath = os.path.join(WORKSPACE_PATH, rel_path)
        if not os.path.exists(self.mod_fpath):
            print(self.mod_fpath)
            raise FileNotFoundError("The model was not downloaded or its files"
                                    " are corrupt. Reinstall it by running: "
                                    f"headctools download {self.model}")
        # UNet, FlapRec
        self.mod, self.pro_han = rel_path.split(os.sep)[0].split('_')

        if self.sh_imgs:
            sh(sitk.ReadImage(self.input_file), 'input-image')

    def preprocess(self):
        valid_prep = ['ct-scan', 'skull']
        vpr_str = ', '.join(valid_prep)

        if str(self.preprocessing).lower() in ['none', '0', 'false']:
            self.prep_fpath = self.input_file
            self.prep_file_folder, self.prep_file_name = os.path.split(
                self.prep_fpath)
        elif self.preprocessing in valid_prep:
            self.prep_file = tempfile.NamedTemporaryFile(suffix='.nii.gz',
                                                         delete=False)
            self.prep_fpath = self.prep_file.name
            self.prep_file_folder, self.prep_file_name = os.path.split(
                self.prep_fpath)

            if self.preprocessing == 'ct-scan':
                prep_img_cr(self.input_file, self.prep_fpath)
            elif self.preprocessing == 'skull':
                prep_img_cr_sk(self.input_file, self.prep_fpath)
        else:
            raise AttributeError(f"The input image method "
                                 f"{self.preprocessing} is not valid. "
                                 f"Choose one from [{vpr_str}] or set it "
                                 f"to 'None' for skipping preprocessing.")

        sh(sitk.ReadImage(self.prep_fpath),
           'prepr-img') if self.sh_imgs else 0

    def run_model(self):
        # PyTorch model
        params = {
            "name": self.model,
            "test_flag": True,
            "model_class": self.mod,
            "problem_handler": self.pro_han,
            "workspace_path": WORKSPACE_PATH,  # It will lookup the model here.
            "single_file": self.prep_fpath,
            "device": DEVICE,
            "resume_model": self.mod_fpath,
            "force_resumed": True,  # Don't use the autogenerated model path
        }
        Model(params=params)

    def load_preds(self):
        self.model = os.path.splitext(os.path.split(self.model)[1])[0]
        out_folder = 'pred_' + self.model

        sk_outs = ['FlapRecDoubleOut', 'FlapRecWithShapePriorDoubleOut']
        self.is_sk = self.pro_han in sk_outs

        fname = os.path.split(self.input_file)[1]  # Input file name
        xt = common.grab_exts(fname)

        self.out_flap = osp.join(self.out_folder_path,
                                 fname.replace(xt, '_fl' + xt))
        self.out_inputsk = osp.join(self.out_folder_path,
                                    fname.replace(xt, '_i' + xt))
        if self.is_sk:
            self.out_fullsk = osp.join(self.out_folder_path,
                                       fname.replace(xt, '_sk' + xt))

        px = [f'_fl{xt}', f'_i{xt}']
        if self.is_sk:
            px.append(f'_sk{xt}')

        pred_flap = os.path.join(self.prep_file_folder, out_folder,
                                 self.prep_file_name.replace(xt, px[0]))

        pred_input = os.path.join(self.prep_file_folder, out_folder,
                                  self.prep_file_name.replace(xt, px[1]))
        if self.is_sk:
            pred_skull = os.path.join(self.prep_file_folder, out_folder,
                                      self.prep_file_name.replace(xt, px[2]))

        # Sum Skull + Flap
        self.fl_stk = sitk.ReadImage(pred_flap, sitk.sitkUInt8)
        self.inp_stk = sitk.ReadImage(pred_input, sitk.sitkUInt8)
        if self.is_sk:
            self.fsk_stk = sitk.ReadImage(pred_skull, sitk.sitkUInt8)

    def postprocess(self):
        if self.mask:
            mask = sitk.ReadImage(self.mask, sitk.sitkUInt8)
            self.fl_stk = sitk.MaskNegated(self.fl_stk, mask)
            if self.is_sk:
                self.fsk_stk = sitk.MaskNegated(self.fsk_stk, mask)

        self.fl_stk = skfn.get_largest_cc(self.fl_stk, rel=self.keep_ratio,
                                          top=self.top)
        if self.is_sk:
            self.fsk_stk = skfn.get_largest_cc(self.fsk_stk,
                                               rel=self.keep_ratio)

        if self.mask and self.is_sk:
            # Since masking could erase the bottom part, we will sum the
            # input image again for restoring that part.
            self.fsk_stk = sitk.Or(self.fsk_stk, self.inp_stk)

        # Transformation to the original space is performed after saving
        # because it needs the written file paths
        # TODO support sitk images and paths using tmp files

        sh(self.fl_stk, 'generated-flap') if self.sh_imgs else 0
        sh(self.inp_stk, 'input-img') if self.sh_imgs else 0
        if self.is_sk:
            sh(self.fsk_stk, 'generated-skull') if self.sh_imgs else 0

    def save_images(self):
        sitk.WriteImage(self.fl_stk, self.out_flap)
        sitk.WriteImage(self.inp_stk, self.out_inputsk)
        if self.is_sk:
            sitk.WriteImage(self.fsk_stk, self.out_fullsk)

        if self.odims:
            undo_reg_in_pred(self.out_flap, prep_id=None, pred_id='_fl',
                             ext='.nii.gz', interp="nearestNeighbor",
                             overwrite=True, reg_id="_reg", tmp_mode=True)
            if self.is_sk:
                undo_reg_in_pred(self.out_fullsk, prep_id=None, pred_id='_sk',
                                 ext='.nii.gz', interp="nearestNeighbor",
                                 overwrite=True, reg_id="_reg", tmp_mode=True)

        self.prep_file.close() if not self.preprocessing else 0


def flap_reconstruction(input_ff: str, out_path: Optional[str] = None,
                        model: str = 'UNetSP', show_intermediate: bool = False,
                        preprocessing: str = 'ct-scan', keep_ratio: float = 1,
                        top: int = -1,
                        image_extension: tuple = ('.nii.gz', '.nrrd'),
                        odims: bool = False, mask=None,
                        include_subfolders=False):
    """ Preprocess the image, predict the bone flap and sum the flap to
    the preprocessed skull.

    This function is a wrapper that instantiates the FlapReconstruction class.

    :param input_ff: Input file or folder.
    :param out_path: Output file or folder path.
    :param model: Trained model name (see which are available trough the
    download option). Default: UNetSP.
    :param show_intermediate: Use SimpleITK for showing intermediate
    :param preprocessing: If the images in the folder are already
    preprocessed, set this flag to True.
    images.
    :param keep_ratio: CC ratio to keep. If it is 1, it will keep only the
    largest CC. Otherwise,  it draws the components that are at least rel % of
    the size of the biggest CC
    :param top: Keep the N biggest connected components. This option overrides
    the keep_ratio option.
    :param image_extension: Possible file extensions of the CT images.
    :param odims: Save the predictions in the original dimensions. A .mat file
    with the applied registration and the original image in the parent folder
    is needed.
    :param mask: Mask to apply to the prediction. This helps to eliminate
    unwanted structures in zones that it shouldn't be any shape.
    :return:
    """
    FlapReconstruction(input_ff, out_path, image_extension, model,
                       show_intermediate, preprocessing, keep_ratio, top, odims,
                       mask, include_subfolders)
