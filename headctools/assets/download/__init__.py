""" Folder containing the downloadable assets URLs."""

import os
import pathlib

# Download folder path
DPATH = str(pathlib.Path(os.path.abspath(os.path.dirname(__file__))))

