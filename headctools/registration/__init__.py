""" Wrappers used in the registration module. """

def register_file_folder(input_ff: str, atlas_path: str, mask_id: str = None,
                         overwrite_files: bool = False, binary: bool = False,
                         method='antspy'):
    """ Wrapper that checks if the path exists and calls the methods above.

    input_ff: str
        File or folder to register.
    atlas_path: str
        Path of the atlas for registering.
    mask_id: str  # TODO add mask registration after image
        If given, it will register the files with that id with the same
        transformation as the file without the id.
    overwrite_files: bool
        Overwrite files in case of existing.
    method: str
        Method to use for registration.
    """
    if method.lower() == 'antspy':
        from .antspy import register_ff
    else:
        from .deformetrica import register_ff

    register_ff(input_ff, atlas_path, mask_id, overwrite_files,
                binary)


def reg_sitk(moving_image, fixed_image=None, mat_save_path=None,
             transform="Rigid", interp="nearestNeighbor",
             mat_to_apply=None, reference=None, method='antspy'):
    """ Wrapper for registering using SimpleITK

    moving_image: str
        File or folder to register.
    fixed_image: str
        Path of the atlas for registering.
    mat_save_path: str
        Path to save the transformation matrix.
    transformation: str
        Type of transformation to use.
    interp: str
        Interpolation method to use.
    mat_to_apply: str
        Path to a matrix to apply to the moving image.
    reference: str
        Path to a reference image.
    method: str
        Method to use for registration.
    """
    if method.lower() == 'antspy':
        from .antspy import register_sitk
    else:
        from .deformetrica import register_sitk

    register_sitk(moving_image, fixed_image, mat_save_path, transform, interp,
                  mat_to_apply, reference)