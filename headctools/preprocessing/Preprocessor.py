""" Preprocessor class

Available presets (check __init__.py for more info):
    - ct_to_skull: registration -> intensities clipping -> normalization ->
    zero thresholding -> resampling -> fixed size conversion -> keep largest cc
    - ct_to_skull_noreg: same as ct_to_skull but without registration.

"""

import os
import shutil
from typing import Optional

import SimpleITK as sitk

from .presets_params import PRESETS, DEFAULT_PARAMS
from .utils import get_niigz_imgs, clip_int_sitk, min_max_normalization, \
    threshold, resample_sz_sp, fixed_pad_sitk, get_largest_cc, save_img, \
    sitk_to_stl_marching_cubes
from .. import utilities as common
from ..registration.antspy import register_sitk


class Preprocessor:
    """Preprocessor class.

    This class is used to preprocess images. It can be used to preprocess
    images for Flap Reconstruction or Segmentation.

    :param input_path: Input path
    :param save_path:
    :param preset:
    """

    def __init__(self, input_path: str, save_path: str = None,
                 preset: str = 'ct_to_skull', file_ids: Optional[dict] = None,
                 params: Optional[dict] = None):
        self.input_path = input_path
        self.input_folder = input_path if os.path.isdir(input_path) else \
            os.path.dirname(input_path)
        self.files = get_niigz_imgs(input_path, file_ids)
        self.preset = preset
        self.params = {}
        self.user_params = params
        self.curr_imgs = {}
        self.save_path = None
        self.tmp_files = []  # Files to be deleted after saving

        if save_path:
            if os.path.splitext(save_path)[1] == "":  # Looks like a folder
                self.save_path = common.veri_folder(save_path)
            else:  # Looks like a file path
                common.veri_folder(os.path.split(save_path)[0])
                self.save_path = save_path

    def run(self, custom_preset: Optional[list] = None,
            u_params: Optional[dict] = None):

        u_params = u_params if u_params is not None else self.user_params

        if custom_preset:
            if not u_params:
                print(" WARNING: You should specify user parameters. Using "
                      "default params for ct_to_skull pipeline.")
                self.params.update(DEFAULT_PARAMS['ct_to_skull'])
            else:
                self.params.update(u_params)

            pipeline = custom_preset
            print(f"Using custom preset: {custom_preset}.")

        elif self.preset not in PRESETS.keys():
            raise AttributeError(
                f"The preset '{self.preset}' is not supported. "
                f"Available presets: {', '.join(PRESETS)}"
            )
        else:
            if 'save_path' in self.params and self.save_path is None:
                self.save_path = self.params['save_path']
            if self.save_path is None:  # If save_path is not specified yet
                folder_name = 'preprocessed_' + self.preset
                self.save_path = os.path.join(self.input_path, folder_name)
                os.makedirs(self.save_path, exist_ok=True)
                print(" No save path specified. Using default save path.")

            pipeline = PRESETS[self.preset]
            self.params.update(DEFAULT_PARAMS[self.preset])  # Default params
            if u_params:
                self.params.update(u_params)  # User params

        self.run_preset(pipeline)

    def load_imgs(self, f):
        self.curr_file = f
        for typ, path in f.items():
            self.curr_imgs[typ] = sitk.ReadImage(path)

    def run_preset(self, pipeline: list):
        for f in self.files:
            print(f"Preprocessing {f}")
            self.load_imgs(f)  # Load the images into curr_imgs and curr_file
            for step in pipeline:
                if step == 'register':
                    self.register()
                elif step == 'clip_intensity_values':
                    self.intens_clip()
                elif step == 'normalize':
                    self.min_max_norm()
                elif step == 'threshold':
                    self.threshold()
                elif step == 'resample':
                    self.resample()
                elif step == 'convert_to_fixed_size':
                    self.convert_to_fixed_size()
                elif step == 'largest_cc':
                    self.largest_cc()
                elif step == 'marching_cubes':
                    self.marching_cubes()
                else:
                    raise AttributeError(
                        f"The step '{step}' is not supported."
                    )
            self.save_imgs()

    def marching_cubes(self):
        print("  Generating .stl of Marching Cubes...")
        self.curr_imgs['marching_cubes'] = sitk_to_stl_marching_cubes(
            self.curr_imgs['image']
        )  # It returns the path of the tmp file
        self.tmp_files.append(self.curr_imgs['marching_cubes'])

    def convert_to_fixed_size(self):
        print("  Padding to fixed size...")

        self.curr_imgs['image'] = fixed_pad_sitk(self.curr_imgs['image'],
                                                 self.params['fixed_size_pad'])

        if 'mask' in self.curr_imgs:
            self.curr_imgs['mask'] = fixed_pad_sitk(
                self.curr_imgs['mask'], self.params['fixed_size_pad']
            )

    def largest_cc(self):
        for label in self.params['largest_cc']:
            if label not in self.curr_imgs:
                raise AttributeError(
                    f"The label '{label}' is not valid. Valid labels: "
                    f"{', '.join(self.curr_imgs.keys())}."
                )
            print(f"  Keeping largest CC of label {label}")
            self.curr_imgs[label] = get_largest_cc(self.curr_imgs[label])

    def resample(self):
        """Resample the image to a given resolution or spacing. """
        resolution = self.params['resample_resolution']
        print(f"  Resampling images...")

        image = self.curr_imgs['image']
        mask = None
        if 'mask' in self.curr_imgs:
            mask = self.curr_imgs['mask']

        image, mask = resample_sz_sp(image, mask, self.params['reg_im_interp'],
                                     self.params['res_target_spacing'],
                                     self.params['res_direction'],
                                     self.params['res_origin'],
                                     self.params['res_target_size'],
                                     )
        self.curr_imgs['image'] = image
        if mask is not None:
            self.curr_imgs['mask'] = mask

    def threshold(self):
        """ Apply a threshold to the image.

        :return:
        """
        value = self.params['threshold']
        print(f"  Thresholding to {threshold}...")
        self.curr_imgs['image'] = threshold(self.curr_imgs['image'], value)

    def min_max_norm(self):
        """Normalize the image to the range [0, 1]"""
        print("  Normalizing to [0, 1]...")
        self.curr_imgs['image'] = min_max_normalization(
            self.curr_imgs['image']
        )

    def intens_clip(self):
        """Clip the intensity values of the image, making values outside that
        range imin-imax.

        :param values: (imin, imax) values for performing the clip.
        """
        if 'clip_intensity_values' not in self.params.keys():
            return None

        values = self.params['clip_intensity_values']
        if len(values) != 2:
            raise AttributeError(
                f"The clip_intensity_values parameter should be a tuple "
                f"with 2 values. Got {values}."
            )

        print("  Intensities clipping...")
        imin, imax = values
        self.curr_imgs['image'] = clip_int_sitk(self.curr_imgs['image'],
                                                imin, imax)

    def register(self):
        if self.params['reg_engine'].lower() == 'ants':
            print("  Registering using ANTsPy...")
        elif self.params['reg_engine'].lower() == 'deformetrica':
            print("  Registering using Deformetrica...")
        else:
            raise AttributeError(
                f"The registration engine '{self.params['reg_engine']}' "
                f"is not supported."
            )

        print(f"    Fixed image: {self.params['reg_atlas_path']}")

        mat_path = None  # In case we don't want to save the matrix
        if self.params['reg_save_transform'] or 'mask' in self.curr_file:
            # path/file.ext1.ext2 -> path/file
            im_path = self.curr_file['image']
            file_w_exts = os.path.split(im_path)[1]
            file_wo_exts = file_w_exts[:-len(common.grab_exts(file_w_exts))]
            mat_path = os.path.join(
                (self.save_path if os.path.isdir(self.save_path) else
                 os.path.split(self.save_path)[0]),
                file_wo_exts + "_reg.mat"
            )
            print(f"    Transformation will be saved in {mat_path}")

        if 'image' in self.curr_file:
            print("    Registering image")
            self.curr_imgs['image'] = register_sitk(
                self.curr_imgs['image'], self.params['reg_atlas_path'],
                mat_path, self.params['reg_transf'],
                self.params['reg_im_interp'], clean_up=True
            )

            if 'mask' in self.curr_file:  # Transform also the mask
                print("    Registering mask")
                self.curr_imgs['mask'] = register_sitk(
                    self.curr_imgs['mask'], mat_to_apply=mat_path,
                    reference=self.curr_imgs['image'],
                    transform=self.params['reg_transf'],
                    interp="nearestNeighbor", clean_up=True
                )

    def save_imgs(self):
        """ Save curr_imgs into self.save_path accordingly

        :return:
        """
        os.makedirs(self.save_path, exist_ok=True)

        # TODO replace 'image' references to variable or null ids.
        orig_filename = os.path.split(self.curr_file['image'])[1]
        if len(self.curr_imgs) > 1:
            for key, val in self.curr_imgs.items():
                saved_fpath = os.path.join(self.save_path,
                                           orig_filename.replace('image', key))
                if type(val) == sitk.Image:
                    save_img(self.curr_imgs[key], saved_fpath)
                elif type(val) == str:
                    m_fpth = saved_fpath.replace('.nii.gz',
                                                 os.path.splitext(val)[1])
                    shutil.copy(val, m_fpth)
        else:
            saved_fpath = os.path.join(self.save_path, orig_filename)
            save_img(self.curr_imgs['image'], saved_fpath)

        print(f"  Saved images in {self.save_path}")

        if len(self.tmp_files):
            print(f"  Deleting temporary files: {self.tmp_files}")
            for f in self.tmp_files:
                os.remove(f)
